package part3;
import java.util.HashMap;


public class WordCounter {
	HashMap<String, Integer> wordCount ;
	private String massage;
	
	public WordCounter(String massage){
		this.massage = massage;
		wordCount = new HashMap<String, Integer>();
		
	}
	
	public void count(){
		int value = 1;
		String[] word = massage.split(" ");
		
		//for(int i = 0; i < s.length; i++){
			//wordCount.put(s[i], value);
		//}
		
		for(int i = 0; i < word.length; i++){
			if(!wordCount.containsKey(word[i])){
				wordCount.put(word[i], value);
				
			}
			else{
				
				wordCount.replace(word[i], wordCount.get(word[i]), wordCount.get(word[i])+value);
			}
			
		}
		
	}
	
	public int hasWord(String word){
		int count = 0;
		if(wordCount.containsKey(word)){
			count = wordCount.get(word);
		}
		return count ;
	}
}
