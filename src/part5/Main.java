package part5;

public class Main {
	Refrigerator ref;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Main main = new Main();
		main.testRefrigerator();
	}
	
	public void testRefrigerator(){
		try{
			ref = new Refrigerator();
			ref.put("apple");
			ref.put("banana");
			ref.put("orange");
			ref.put("cola");
			ref.put("orange juice");
			ref.put("ice");
			ref.put("ice-cream");
			ref.put("chocolate");
			ref.put("candy");
			ref.put("milk");
			System.out.println(ref.toString());
			System.out.println("-----------------take out cola---------------------");
			ref.takeOut("cola");
			System.out.println(ref.toString());
			ref.put("cola");
			System.out.println(ref.toString());
			System.out.println("put over 10");
			ref.put("tomato");
			ref.put("tomato");
			System.out.println(ref.toString());
		}
		
		catch(FullException e){
			System.err.println("Error : " + e.getMessage());
		}

		
	}

}
