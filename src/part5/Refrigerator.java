package part5;

import java.util.ArrayList;

public class Refrigerator {
	int size;
	private ArrayList<String> things;
	public Refrigerator(){
		things = new ArrayList<String>();
		size = 0;
	}
	
	public void put(String stuff) throws FullException{
		if(size > 10){
			throw new FullException("Cannot put this stuff");
		}
		things.add(stuff);
		size++;
		
	}
	
	public String takeOut(String stuff){
		String thing = null;
		for(int i = 0; i < size; i++){
			if(things.get(i).equals(stuff)){
				thing = stuff;
				things.remove(i);
				size--;
				break;
			}
		}
		return thing;
	}
	
	public String toString(){
		String thing = "";
		for(String s: things){
			thing = thing + " " + s + " ";
		}
		thing = thing.trim();
		return thing + " " + size;
	}
}
