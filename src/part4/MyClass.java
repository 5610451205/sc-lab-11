package part4;

public class MyClass {
	
	public void methX() throws DataExcption{
		//...code...
		throw new DataExcption("It was data error");
	}
	
	public void methY() throws FormatExcption{
		//...code...
		throw new FormatExcption();
	}
}
