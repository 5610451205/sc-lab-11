package part4;

public class MyClassMain {

	public static void main(String[] args) {
		try{
			MyClass c = new MyClass();
			System.out.println("A");
			c.methX();
			System.out.println("B");
			c.methY();
			System.out.println("C");
		}catch(DataExcption e){
			System.out.println("D");
		}catch(FormatExcption e){
			System.out.println("E");
		}finally{
			System.out.println("F");
		}
		System.out.println("G");
	}

}
